
#include <complex.h>
#include <iostream>
#include <list>
#include <memory>
#include <string>
class HashTable;

using namespace std;

class HashKey
{
public:
    string Key;
    int Data;
    HashKey* NextLink = nullptr;

    bool IsEmpty()
    {
        return Key.length() == 0;
    }

    void FillData(string Key, int Data, HashKey* NextLink = nullptr)
    {
        this->Key = Key;
        this->Data = Data;
        this->NextLink = NextLink;
    }

    void InsertKey(HashKey* ExeptionKey)
    {
        if (NextLink != nullptr && NextLink != ExeptionKey)
        {
            NextLink->InsertKey(ExeptionKey);
        }
        else
        {
            NextLink = ExeptionKey->NextLink;
        }
    }

    HashKey* Get(string Key)
    {
        if (IsEmpty())
        {
            return nullptr;
        }

        return Key == this->Key ? this : NextLink != nullptr ? NextLink->Get(Key) : nullptr;
    }

    void Print()
    {
        cout << Key << endl;

        if (NextLink != nullptr)
        {
            NextLink->Print();
        }
    }
};

class HashTable
{
public:
    void Add(string Key, int Data);
    int Get(string Key);
    bool Delete(string Key);
    void ReHesh();
    HashTable();
    ~HashTable();
private:
    size_t Capacity = 5;
    HashKey** HashKeys;

    int GetHashCode(string Key);

    HashKey* GetKeyRef(string Key, int* KeyIndex);

    int GetIndex(string Key);
    int Hash(int Count) const { return Count % Capacity; }


    void UpdateHashKeys(HashKey* CurrentHashKey, HashKey** CopyHashKeys);
};


void HashTable::Add(string Key, int Data)
{
    int Index = GetIndex(Key);

    if (HashKeys[Index]->IsEmpty())
    {
        HashKeys[Index]->FillData(Key, Data);
    }
    else
    {
        auto NewKey = new HashKey;
        NewKey->FillData(Key, Data, HashKeys[Index]);
        HashKeys[Index] = NewKey;
    }
}

int HashTable::GetHashCode(string Key)
{
    int Count = 0;
    for (auto Symbol : Key)
    {
        Count += static_cast<int>(Symbol);
    }
    return Count;
}

int HashTable::Get(string Key)
{
    int Index;
    HashKey* KeyRef = GetKeyRef(Key, &Index);
    return KeyRef != nullptr ? KeyRef->Data : NULL;
}

HashKey* HashTable::GetKeyRef(string Key, int* KeyIndex)
{
    *KeyIndex = GetIndex(Key);
    return HashKeys[*KeyIndex]->Get(Key);
}

bool HashTable::Delete(string Key)
{
    int Index = 0;
    HashKey* KeyRef = GetKeyRef(Key, &Index);
    if (KeyRef == nullptr)
    {
        return false;
    }

    HashKey* NextKeyRef = KeyRef->NextLink;

    if (KeyRef == HashKeys[Index])
    {
        HashKeys[Index] = NextKeyRef != nullptr ? NextKeyRef : new HashKey;
    }
    else
    {
        HashKeys[Index]->InsertKey(KeyRef);
    }

    free(KeyRef);
    return true;
}

int HashTable::GetIndex(string Key)
{
    return Hash(GetHashCode(Key));
}

HashTable::HashTable()
{
    HashKeys = new HashKey*[Capacity];
    for (int i = 0; i < Capacity; i++)
    {
        HashKeys[i] = new HashKey;
    }
}

HashTable::~HashTable()
{
    delete HashKeys;
    HashKeys = nullptr;
}


void HashTable::UpdateHashKeys(HashKey* CurrentHashKey, HashKey** CopyHashKeys)
{
    int Index = GetIndex(CurrentHashKey->Key);
    HashKey* NextLink = CurrentHashKey->NextLink;

    CurrentHashKey->NextLink = !CopyHashKeys[Index]->IsEmpty() ? CopyHashKeys[Index] : nullptr;
    CopyHashKeys[Index] = CurrentHashKey;

    if (NextLink != nullptr)
    {
        UpdateHashKeys(NextLink, CopyHashKeys);
    }
}

void HashTable::ReHesh()
{
    size_t PreviousCapacity = Capacity;
    Capacity = Capacity * 2;

    auto CopyHashKeys = new HashKey*[Capacity];
    for (int i = 0; i < Capacity; i++)
    {
        CopyHashKeys[i] = new HashKey;
    }

    for (int i = 0; i < PreviousCapacity; i++)
    {
        if (HashKeys[i]->IsEmpty())
        {
            continue;
        }
        UpdateHashKeys(HashKeys[i], CopyHashKeys);
    }

    move(CopyHashKeys, CopyHashKeys + Capacity, HashKeys);
}


int main(int argc, char* argv[])
{
    auto HashRef = new HashTable;
    HashRef->Add("dot", 84);
    HashRef->Add("god", 505);
    HashRef->Add("odg", 148);
    HashRef->Add("dog", 22);
    HashRef->Add("moi", 99);
    HashRef->Add("mod", 44);


    return 0;
}
